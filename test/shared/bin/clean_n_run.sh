#!/bin/bash

##############################################
function separator
{ 
  line="-------------------------------"
  echo 
  echo "$line"
  echo "$1"
  echo "$line"
}
##############################################

separator "cleaning up : "
echo "...removing the node_modules dir..."
rm -rf ./node_modules/ 
echo "done."

separator "re-install dependencies"
yarn install

separator "test result below"
node app.js
echo

separator "final cleaning up"
rm -rf ./node_modules/ 

