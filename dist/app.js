"use strict";

// require packages here
//const dallmo_got = require("dallmo-got");
import {dallmo_got} from 'dallmo-got';

// the api endpoint for random string requests
const api_endpoint = "https://baconipsum.com/api/?type=any";

/////////////////////////////////////////////////////////////////////
// main functions
async function dallmo_bacon_ipsum(){

  // response body is an array
  let content;

  // in case any error in getting response from the api endpoint
  const content_error = ['error in getting response from endpoint'];

  //-------------------------------------------------
  try{
    // res : http request response
    const res = await dallmo_got.get( api_endpoint );

    if( res.status_code == 200 ){
      content = res.body;
    }else{
      throw( "error in response status code : ", res.status_code );
    }//if else, check response status_code
  }catch(e){
    //console.log( "error message : ", e );
    console.log( "caught error code from GOT : ", e.code );
    content = content_error ;
  }//try catch for errors
  //-------------------------------------------------

  // generate a random position in the response body array
  const random_pos = Math.floor( Math.random() * content.length );
  const return_content = content[ random_pos];

  // resolve / return the random string here
  return( return_content );        

}//function random_string
/////////////////////////////////////////////////////////////////////
// all exports go here
export {
  dallmo_bacon_ipsum,
}; //
