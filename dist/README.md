# dallmo-bacon-ipsum

- a simple wrapper based on the json api by [bacon ipsum][link-bacon-ipsum], and output only the random texts for direct use.
- ESM-native
- using this in CJS has to be done via [dynamic import][ref-2] 

# usage
the package export a promise, which will resolve either to the random words by [bacon ipsum][link-bacon-ipsum], or related error messages.  

## cjs
```
"use strict";

( async()=>{

  const dbi = await import("dallmo-bacon-ipsum");
  
  console.log( await dbi.dallmo_bacon_ipsum() );
  console.log( await dbi.dallmo_bacon_ipsum() );

})(); // self-run async main 
```

## esm
```
"use strict";

import {dallmo_bacon_ipsum} from 'dallmo-bacon-ipsum';

  console.log( await dallmo_bacon_ipsum() );
  console.log( await dallmo_bacon_ipsum() );
```



[ref-1]: https://redfin.engineering/node-modules-at-war-why-commonjs-and-es-modules-cant-get-along-9617135eeca1
[ref-2]: https://v8.dev/features/dynamic-import
[link-bacon-ipsum]: https://baconipsum.com/json-api/

